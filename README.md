main development branch (Master)
- the most up to date working codebase
- only merge working code to the main branch! (small bugs are fine,  be sure to note them or create new tickets)

other branches
- make them as we need, used to solve tickets or experiment without affecting main branch

guide
- create new branch:
	- git branch "branch-name"
- switch to branch:
	- checkout "branch-name"
- create and switch to new branch:
	- git checkout -b "branch-name"
- merge branches:
	- git checkout "main branch"
	- git merge "branch-to-be-merged"

Mitchell has joined this repo :)
